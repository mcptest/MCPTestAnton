package mcptest;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MCPTest1 {

  
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    
    System.out.println("Plean Enter Max Array Length");
    int length = sc.nextInt();
    int[] arrayNums = new int[length];
    System.out.println("Plean Enter Array Element");
    for (int i = 0; i < length; i++) {
      arrayNums[i] = sc.nextInt();
    }
        
    
    List<Integer> arrResult = checkSubtracted(arrayNums);
    
    System.out.println("RESULT : ");
    for (int i=0;i<arrResult.size();i++) {
      int numResult = arrResult.get(i);
      if(i == arrResult.size()-1)
        System.out.print(numResult);
      else
        System.out.print(numResult + ",");
    }
  }
  
  private static List<Integer> checkSubtracted(int[] nums) {
    List<Integer> arrResult = new ArrayList();
    boolean isTrue = false;    
    for (int i=0;i<nums.length;i++) {
      for (int j=0;j<nums.length;j++) {
        if (i != j) {
          if (nums[i] - nums[j] > 0) {
            isTrue = true;            
          }
          else {
            isTrue = false;
            break;
          }
        }
      }
      
      if (isTrue)
        arrResult.add(nums[i]);
    }
    
    return arrResult;
  }
  
}
