package mcptest;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MCPTest3 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    
    System.out.println("Plean Enter The Word");
    String word = sc.nextLine();
    
    System.out.println("Plean Enter Integer X");
    int x = sc.nextInt();
    
        
    
    List<String> arrResult = checkWordLength(word, x);
    
    System.out.println("RESULT : ");
    for (int i=0;i<arrResult.size();i++) {
      String strResult = arrResult.get(i);
      if(i == arrResult.size()-1)
        System.out.print(strResult);
      else
        System.out.print(strResult + " , ");
    }
  }
  
  private static List<String> checkWordLength(String word, int x) {
    List<String> arrResult = new ArrayList();
    boolean isTrue = false;    
    String [] arrString = word.split(" ");
    
    for (int i=0; i<arrString.length; i++) {
      if (arrString[i].length() == x)
        arrResult.add(arrString[i]);
    }
    
    return arrResult;
  }
}
