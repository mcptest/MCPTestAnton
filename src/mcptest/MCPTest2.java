package mcptest;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MCPTest2 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    
    System.out.println("Plean Enter Max Array Length");
    int length = sc.nextInt();
    
    System.out.println("Plean Enter X number");
    int x = sc.nextInt();
    int[] arrayNums = new int[length];
    System.out.println("Plean Enter Array Element");
    for (int i = 0; i < length; i++) {
      arrayNums[i] = sc.nextInt();
    }
        
    
    List<Integer> arrResult = checkDivided(arrayNums, x);
    
    System.out.println("RESULT : ");
    for (int i=0;i<arrResult.size();i++) {
      int numResult = arrResult.get(i);
      if(i == arrResult.size()-1)
        System.out.print(numResult);
      else
        System.out.print(numResult + ",");
    }
  }
  
  private static List<Integer> checkDivided(int[] nums, int x) {
    List<Integer> arrResult = new ArrayList();
    boolean isTrue = false;    
    for (int i=0;i<nums.length;i++) {
      for (int j=0;j<nums.length;j++) {
        if ((nums[i] / nums[j]) == x) {
          isTrue = false;
          break;
        }
        else {
          isTrue = true;            
        }        
      }
      
      if (isTrue)
        arrResult.add(nums[i]);
    }
    
    return arrResult;
  }
}
